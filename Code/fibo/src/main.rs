use std::io;

fn fib_iter(n: u32) -> u32 {
    if n == 0 {
        return 0;
    } else if n == 1 {
        return 1;
    }

    let mut fib_n1: u32 = 1;
    let mut fib_n2: u32 = 0;
    let mut fib_n: u32 = 0;

    for _ in 2..n + 1 {
        fib_n = fib_n1 + fib_n2;
        fib_n2 = fib_n1;
        fib_n1 = fib_n;
    }

    return fib_n;
}

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");
    let n: u32 = input.trim().parse().ok().expect("Please type a number!");

    println!("fib({}) = {}", n, fib_iter(n));
}
