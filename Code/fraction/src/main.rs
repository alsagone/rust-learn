fn gcd(x: i32, y: i32) -> i32 {
    let mut r: i32;
    let mut a = if x > y { x } else { y };
    let mut b = if a == x { y } else { x };

    while b > 0 {
        r = a % b;
        a = b;
        b = r;
    }

    a
}

struct Fraction {
    numerator: i32,
    denominator: i32,
}

impl Fraction {
    fn new(num: i32, denom: i32) -> Fraction {
        if denom == 0 {
            panic!("The denominator can't be equal to zero!");
        }

        Fraction {
            numerator: num,
            denominator: denom,
        }
    }

    fn to_string(&self) -> String {
        format!("{}/{}", self.numerator, self.denominator)
    }

    fn get_value(&self) -> f32 {
        self.numerator as f32 / self.denominator as f32
    }

    fn simplify(&self) -> Fraction {
        let g = gcd(self.numerator, self.denominator);
        Fraction::new(self.numerator / g, self.denominator / g)
    }

    fn opposite(&self) -> Fraction {
        Fraction::new(-1 * self.numerator, self.denominator)
    }

    fn inverse(&self) -> Fraction {
        Fraction::new(self.denominator, self.numerator)
    }
}

fn add(frac: &Fraction, other_fraction: &Fraction) -> Fraction {
    let g = gcd(frac.denominator, other_fraction.denominator);
    let lowest_common_denominator = frac.denominator * other_fraction.denominator / g;

    let numerator = (frac.numerator * other_fraction.denominator
        + other_fraction.numerator * frac.denominator)
        / g;

    Fraction::new(numerator, lowest_common_denominator)
}

fn substract(frac: &Fraction, other_fraction: &Fraction) -> Fraction {
    let opposite = other_fraction.opposite();
    add(&frac, &opposite)
}

fn multiply(frac: &Fraction, other_fraction: &Fraction) -> Fraction {
    let numerator = frac.numerator * other_fraction.numerator;
    let denominator = frac.denominator * other_fraction.denominator;
    Fraction::new(numerator, denominator).simplify()
}

fn divide(frac: &Fraction, other_fraction: &Fraction) -> Fraction {
    let inv = other_fraction.inverse();
    multiply(&frac, &inv)
}

fn add_number(frac: &Fraction, n: i32) -> Fraction {
    let other_fraction = Fraction::new(n, 1);
    add(&frac, &other_fraction)
}

fn main() {
    let f = Fraction::new(1, 3);
    let g = Fraction::new(3, 4);
    let addition = add(&f, &g);
    let sub = substract(&f, &g);
    let mult = multiply(&f, &g);
    let div = divide(&f, &g);
    let add_n = add_number(&f, 5);

    println!(
        "{}; {}; {}; {}; {}; {}",
        f.get_value(),
        addition.to_string(),
        sub.to_string(),
        mult.to_string(),
        div.to_string(),
        add_n.to_string()
    )
}
