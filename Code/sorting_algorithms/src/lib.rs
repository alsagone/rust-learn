pub mod vecteur;

#[cfg(test)]
mod tests {
    #[test]
    fn test_initialisation() {
        let v = super::vecteur::vecteur_aleatoire(20);
        assert_eq!(v.len(), 20);
    }

    #[test]
    fn test_verification_tri() {
        let vec_trie = super::vecteur::vecteur_trie(20);
        let vec_non_trie = super::vecteur::vecteur_aleatoire(20);

        assert_eq!(super::vecteur::est_trie(&vec_trie), true);
        assert_eq!(super::vecteur::est_trie(&vec_non_trie), false);
    }

    #[test]
    fn test_tri_bulle() {
        let vec_non_trie = super::vecteur::vecteur_aleatoire(20);
        let vec_trie = super::vecteur::tri_bulle(&vec_non_trie);
        assert_eq!(super::vecteur::est_trie(&vec_trie), true);
    }

    #[test]
    fn test_tri_selection() {
        let vec_non_trie = super::vecteur::vecteur_aleatoire(20);
        let vec_trie = super::vecteur::tri_selection(&vec_non_trie);
        assert_eq!(super::vecteur::est_trie(&vec_trie), true);
    }

    #[test]
    fn test_tri_fusion() {
        let vec_non_trie = super::vecteur::vecteur_aleatoire(20);
        let vec_trie = super::vecteur::tri_fusion(&vec_non_trie);
        assert_eq!(super::vecteur::est_trie(&vec_trie), true);
    }
}
