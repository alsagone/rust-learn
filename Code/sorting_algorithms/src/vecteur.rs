use rand::seq::SliceRandom;
use rand::thread_rng;
use std::cmp::Ordering;

pub fn vecteur_trie(nb_elements: u32) -> Vec<u32> {
    (0..nb_elements).collect()
}

pub fn vecteur_aleatoire(nb_elements: u32) -> Vec<u32> {
    let mut vec: Vec<u32> = vecteur_trie(nb_elements);
    vec.shuffle(&mut thread_rng());
    vec
}

pub fn est_trie(vec: &Vec<T>) -> bool {
    let mut b = true;

    for i in 0..&vec.len() - 1 {
        if vec[i].cmp(&vec[i + 1]) == Ordering::Greater {
            b = false;
            break;
        }
    }

    b
}

pub fn tri_bulle(vec: &Vec<T>) -> Vec<T> {
    let mut vec_trie = vec.to_vec();
    let mut fin = false;

    while !fin {
        fin = true;
        for i in 0..vec_trie.len() - 1 {
            if vec_trie[i].cmp(&vec_trie[i + 1]) == Ordering::Greater {
                vec_trie.swap(i, i + 1);
                fin = false;
            }
        }
    }

    vec_trie
}

pub fn tri_selection(vec: &Vec<u32>) -> Vec<u32> {
    let mut vec_trie = vec.to_vec();
    let mut fin = false;
    let mut dernier_index = vec.len() - 1;

    while !fin {
        fin = true;

        for i in 0..dernier_index {
            if vec_trie[i].cmp(&vec_trie[i + 1]) == Ordering::Greater {
                vec_trie.swap(i, i + 1);
                fin = false;
            }
        }

        dernier_index -= 1;
    }

    vec_trie
}

fn fusion(vec_gauche: &Vec<T>, vec_droite: &Vec<T>) -> Vec<T> {
    let mut i = 0;
    let mut j = 0;
    let mut vec_fusionne: Vec<T> = Vec::new();

    while i < vec_gauche.len() && j < vec_droite.len() {
        if vec_gauche[i].cmp(&vec_droite[j]) == Ordering::Less {
            vec_fusionne.push(vec_gauche[i]);
            i += 1;
        } else {
            vec_fusionne.push(vec_droite[j]);
            j += 1;
        }
    }

    while i < vec_gauche.len() {
        vec_fusionne.push(vec_gauche[i]);
        i += 1;
    }

    while j < vec_droite.len() {
        vec_fusionne.push(vec_droite[j]);
        j += 1;
    }

    vec_fusionne
}

pub fn tri_fusion(vec: &Vec<T>) -> Vec<T> {
    if vec.len() < 2 {
        return vec.to_vec();
    }

    let milieu = vec.len() / 2;
    let vec_gauche = vec[..milieu].to_vec();
    let vec_droite = vec[milieu..].to_vec();

    let vec_gauche_trie = tri_fusion(&vec_gauche);
    let vec_droite_trie = tri_fusion(&vec_droite);
    fusion(&vec_gauche_trie, &vec_droite_trie);
}
