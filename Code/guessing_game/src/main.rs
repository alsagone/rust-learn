extern crate rand;

use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");
    let mut min = 1;
    let mut max = 100;

    let secret_number = rand::thread_rng().gen_range(min, max + 1);

    loop {
        if min == max {
            println!("You win ! -> the secret number was {}", secret_number);
            break;
        }

        println!("Guess a number between {} and {}", min, max);
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guessed_number: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        if guessed_number < min || max < guessed_number {
            println!("Number is out of range !");
        } else {
            match guessed_number.cmp(&secret_number) {
                Ordering::Less => {
                    println!("Too small !");
                    min = guessed_number + 1;
                }
                Ordering::Greater => {
                    println!("Too big !");
                    max = guessed_number - 1;
                }
                Ordering::Equal => {
                    println!("You win !");
                    break;
                }
            }
        }
    }
}
