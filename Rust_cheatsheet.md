# Memo Rust

## Bases

- Nouveau projet Cargo
  `cargo new projet --bin && cd projet`

- Compiler + exécuter un projet Cargo
  `cargo build && cargo run`

- Fonction main

  ```rust
  fn main() {
    println!("Hello world!");
  }
  ```

## Variables

Équivalement de let et const en TypeScript

```rust
let mut mutable_string = "abc"
mutable_string = "def" //Ok

let x = 5;
x = 6 //Erreur
```

Possible de déclarer une variable et son type comme en TypeScript

```rust
let mut c: char = 'c';
let x: i32 = 256;
let result: f64 = 10.00;
let b: bool = true;
const MAX_POINTS: u32 = 100_000;
```

Par contre, pas possible de changer le type d'une variable, même si elle a été déclarée mutable.

```rust
let mut spaces = "   ";
spaces = spaces.len(); //Erreur
```

## Concept de _shadowing_

Rust se débrouille tout seul pour déterminer le type d'une variables.

```rust
let spaces = "   ";
let spaces = spaces.len() //OK
```

## Afficher une variable dans une string

```rust
println!("x = {}", x);
```

## Lire une input et la parse en un int

```rust
use std::io;
let mut input = String::new();
io::stdin()
  .read_line(&mut input)
  .expect("Failed to read line")

let n: u32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
```

## Récupérer le résultat d'une comparaison

```rust
use std::cmp::Ordering;

let a = 8;
let b = 5;

match a.cmp(&b) {
    Ordering::Less => {
      println!("a < b");
    }

    Ordering::Greater => {
        println!("a > b");
    }

    Ordering::Equal => {
      println!("a = b");
    }
}
```

## Générer un nombre aléatoire

Ajouter `rand = "0.3.14"` dans les dépendances du fichier `Cargo.toml`
Faire un `cargo build` pour télécharger la nouvelle dépendance

```rust
extern crate rand;
use rand::Rng;

//Générer un nombre dans l'intervalle [1; 100]
let min = 1;
let max = 100;
let random_number = rand::thread_rng().gen_range(min, max + 1);

```

## Tuples

```rust
let tup: (i32, f64, u8) = (500, 6.4, 1);
let (x, y, z) = tup;
println!("y = {}", y);

println!("First value: {}", tup.0);
println!("Second value: {}", tup.1);
println!("Third value: {}", tup.2);
```

## Tableaux

```rust
let arr = [1,2,3,4,5];

for element in arr.iter() {
  println!("{}", element);
}
```

On accède aux éléments comme dans tout langage de programmation `arr[0]`
Contrairement aux tuples, tous les éléments doivent être du même type
Les tableaux ont une taille unique, une fois déclarés, on ne peut rien ajouter/supprimer
-> alternative Vecteurs

## Fonctions

```rust
fn add (x: i32, y: i32) -> i32 {
  return x + y;
}

fn main() {
  println!("3 + 5 = {}", add(3, 5));
}
```

D'ailleurs, pas besoin de faire un `return variable` à chaque fin de fonction, Rust renvoie automatiquement la valeur de la dernière expression calculée.

```rust
fn add_one(x: i32) -> i32 {
  x + 1
}

fn main() {
  const x = 5;
  const y = add_one(x);
  println!("{} + 1 = {}", x, y);
}
```

## Boucles _for_

Comme en Bash, `for i in 1..4` va de 1 à 3.

Pour renverser la range:

```rust
for number in (1..4).rev() {
  println!("{}!", number);
}
// Affiche 3 2 1
```

## Assignement conditionnel

```rust
let number = if (condition) { 5 } else { 6 };
```

## _Ownership_

Chaque valeur en Rust a une variable qui s'appelle _l'owner_
Qu'un seul _owner_ à la fois
Quand _l'owner_ est _out of scope_, la valeur est _dropped_

```rust
// ...
{
  let s = "hello";
}

//Provoque une erreur parce que s est en dehors du scope
println!(s);
```

Rust se sert de ça pour gérer la mémoire.
Il malloc/free automatiquement les variables quand il faut donc on n'a pas à gérer ça comme en C par exemple.

Par exemple, le type String

```rust
{
  let s = String::from("hello");

  //Ce type de string est mutable
  s.push_str(", world!");

  println!("{}", s);
}

//À partir de ce point, s est out of scope
// Et Rust free automatiquement la mémoire allouée
```

Et du coup, on ne peut pas faire quelque chose du style

```rust
{
  let s1 = String::from("hello");
  let s2 = s1;
}
```

Parce que faire `s2 = s1` ne copie pas le contenu de la string, s2 pointe juste sur s1.
Et du coup quand s1 et s2 se retrouvent out of scope, Rust essaie de free deux fois la même zone mémoire, ce qui peut provoquer une corruption de mémoire et entraîner des problèmes de sécurité.
(De toute façon, Rust affiche une erreur à la compilation si jamais on essaie de compiler ce bout de code)

À la place, on a la méthode clone

```rust
{
  let s1 = String::from("hello");
  let s2 = s1.clone();
  println!("s1 = {}, s2 = {}", s1, s2);
}
```

Possible de retourner plusieurs valeurs dans une fonction avec un tuple

```rust
 fn calculate_length(s: String) -> (String, usize) {
   let length = s.len();
   (s, length)
 }

 fn main() {
   let s1 = String::from("hello");
   let (s2, len) = calculate_length(s1);
   println!("The length of '{}' is {}", s2, len);
 }
```

## Références

Mais c'est une manière trop compliquée de faire ça, à la place on peut utiliser le concept de "référence"

```rust
fn calculate_length(&s: String) -> usize {
  s.len();
}

let s1 = String::from("hello");
let len = calculate_length(&s1);
```

On peut muter les références mais ça demande au préalable que la variable référencée soit mutable.

On ne peut pas avoir plusieurs références mutables d'une même variable dans le même scope.

```rust
fn main() {
  let mut s = String::from("Hello");  //s est set comme mutable
  let r1 = &mut s;
  let r2 = &mut s; //ERREUR
}
```

Ni avoir un mélange de références mutables et immutables.

```rust
let mut s = String::from("hello");
let r1 = &s; //Ok
let r2 = &s; //Ok
let r3 = &mut s; //ERREUR
```

### Références "dans le vide"

Dans des langages qui utilisent des pointeurs, il est facile de se retrouver avec des pointeurs qui pointent vers une zone mémoire qui a potentiellement été donnée à quelqu'un d'autre en libérant la zone mémoire tout en préservant le pointeur.

Par contre en Rust, le compilateur vérifie de lui-même si on fait une référence à rien du tout.
Par exemple, ce code-là renvoie une erreur de compilation.

```rust
fn reference_vide() -> &String {
  let s = String::from("hello");
  &s
}

fn main() {
  let ref_vide = reference_vide();
}
```

```
Error[E0106]: missing lifetime specifier
fn reference_vide() -> &String
= help: this function's return type contains a borrowed value, but there is no value for it to be borrowed from
= help: consider giving it a 'static lifetime
```

Parce que quand la fonction `reference_vide` se finit, s est _out of scope_ donc la référence n'est plus valable.

### Slices

Ça fonctionne comme en Python:

- `&s[1..5]` renvoie tout ce qu'il y a de l'indice 1 à 4 inclus.
- `&s[..5]` est équivalent à `&s[0..5]`
- `&s[3..]` est équivalent à `&s[3..s.len()]`

```rust
let s = String::from("Hello world !");
let hello = &s[0..5]; //Hello
```

Écrire une fonction qui retourne le premier mot dans une string.

```rust
fn premier_mot(s: &str) -> &str {
  let bytes = s.as_bytes();
  let mut slice_index = s.len();

  //Équivalent de for const [index, value] of arr.entries() en JS

  for (index, &value) in bytes.iter().enumerate() {
    //Si on trouve un espace, on s'arrête
    if value == b' ' {
      slice_index = index;
      break;
    }
  }

  return &s[0..slice_index];
}

fn main() {
    let s = String::from("Hello world !");
    println!("{}", premier_mot(&s));
}
```

## Structs

```rust
struct User {
  username: String,
  email: String,
  sign_in_count: u64,
  active: bool
}

let user1 = User {
  email: String::from("someone@example.com"),
  username: String::from("someusername123"),
  sign_in_count: 1,
  active: true,
};

//On peut utiliser ..user1 pour dire à Rust d'utiliser les champs d'user 1
let user2 = User {
  email: String::from("another@example.com"),
  username: String::from("anotherusername567"),
  ..user1
};

//Constructeur
fn build_user(email: String, username: String) -> User {
  User {
    email: email,
    username: username,
    sign_in_count: 1,
    active: true,
  }
}

//On peut aussi faire des tuples structs
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

let black = Color(0, 0, 0);
let origin = Point(0, 0, 0);
```

On peut print les contenus d'une struct avec `:?` mais il faut ajouter `#[derive(Debug)]` juste au-dessus de la définition de la struct

```rust
#[derive(Debug)]
struct Rectangle {
  longueur: u32,
  largeur: u32
}

let rect1 = Rectangle { length: 50, width: 30 };
println!("rect1 is {:?}", rect1);

// rect1 is Rectangle { length: 50, width: 30 }
```

Exemple

```rust
#[derive(Debug)]
struct Rectangle {
  longueur: u32,
  largeur: u32
}

// Implementation block, contient toutes les méthodes
impl Rectangle {
  fn aire(&self) -> u32 {
    self.longueur * self.largeur
  }

  fn peut_contenir(&self, autre: &Rectangle) -> bool {
    self.longueur > autre.longueur && self.largeur > autre.largeur
  }

  // à appeler avec Rectangle::carre(taille)
  fn carre(taille: u32) -> Rectangle {
    Rectangle {longueur: taille, largeur: taille}
  }
}

fn main() {
  let rect1 = Rectangle {longueur: 50, largeur: 30};
  let rect2 = Rectangle {longueur: 20, largeur: 10};
  let carre = Rectangle::carre(10);
  println!("Aire rect1: {}", rect1.aire());
  println!("rect1 peut contenir rect2 ? {}", rect1.peut_contenir(&rect2));
  println!("{:?}", carre);
}
```

## Enums

```rust
enum IpAddrKind {
  V4,
  V6
}

//Utiliser des enums en tant que paramètres
fn route(ip_type: IpAddrKind) {}

route(IpAddrKind::V4);
route(IpAddrKind::V6);

//Utiliser des enums dans des structs
struct IpAddr {
  kind: IpAddrKind,
  adress: String,
}

let home = IpAddr {
  kind: IpAddrKind::V4,
  address: String::from("127.0.0.1"),
};

let loopback = IpAddr {
  kind: IpAddrKind::V6,
  address: String::from("::1"),
};
```

On peut associer des valeurs à des enums
Par exemple au lieu de faire un enum IP V4/V6 et une struct AdresseIP(type, adresse),
on peut directement attacher l'adresse à l'enum.

```rust
enum IpAddr {
  V4(String),
  V6(String),
}

let home = IpAddr::V4(String::from("127.0.0.1"));
let loopback = IpAddr::V6(String::from("::1"));
```

Autre avantage, ça permet de faire des variantes.
Par exemple, si on veut stocker les 4 nombres d'une adresse IPV4 mais directement avoir la string en IPV6, ça se fait.

```rust
enum IpAddr {
  V4(u8, u8, u8, u8),
  V6(String),
}

let home = IpAddr::V4(127, 0, 0, 1);
let loopback = IpAddr::V6(String::from("::1"));
```

Autre exemple

```rust
enum Message {
  Quit,
  Move { x: i32, y: i32 },
  Write(String),
  ChangeColor(i32, i32, i32),
}

impl Message {
  fn call(&self) {
  // method body would be defined here
  }
}


struct QuitMessage; // unit struct
struct MoveMessage {
  x: i32,
  y: i32,
}
struct WriteMessage(String); // tuple struct
struct ChangeColorMessage(i32, i32, i32); // tuple struct


let m = Message::Write(String::from("hello"));
m.call();
```

## Match

```rust
enum Argent {
  Billet { valeur: u32 },
  Piece { valeur: u32 },
  Centime { valeur: u32 }
}

fn valeur_en_centimes(a: Argent) -> u32 {
    let centimes: u32;
    match a {
      Argent::Centime{ valeur } => centimes = valeur,
      Argent::Piece{ valeur } => centimes = valeur * 100,
      Argent::Billet{ valeur } => centimes = valeur * 100
    }

  centimes
}

fn main() {
  let a = Argent::Billet {valeur: 5};
  println!("{}", valeur_en_centimes(a));
}
```

### Match avec `Option<T>`

Exemple: écirre une fonction qui prend un `Option<i32>` en paramètre
S'il y a une valeur à l'intérieur, lui ajouter un
Sinon retourner None

```rust
fn plus_one(x: Option<i32>) -> Option<i32> {
  match x {
    None => None,
    Some(i) => Some(i+1),
  }
}

let five = Some(5);
let six = plus_one(five);
let none_element = plus_one(None);
```

### Placeholder

```rust
let some_u8_value = 0u8;

match some_u8_value {
  1 => println!("one"),
  3 => println!("three"),
  5 => println!("five"),
  7 => println!("seven"),
  _ => (), //Sinon, ne rien faire
}
```

### if... let... else

```rust
#[derive(Debug)]
enum UsState {
  Alabama,
  Alaska,
}

enum Coin {
  Penny,
  Nickel,
  Dime,
  Quarter(UsState),
}

let coin = Coin::Penny;
let mut count = 0;

match coin {
  Coin::Quarter(state) => println!("State quarter from {:?}!", state),
  _ => count += 1,
}

# est équivalent à

let mut count = 0;

if let Coin::Quarter(state) = coin {
  println!("State quarter from {:?}!", state);
} else {
  count += 1;
}
```

## Modules

Créer un module avec `cargo new nom_module` (la commande de d'habitude mais sans le flag `--bin`)
Ça crée un fichier `src/lib.rs`

```rust
mod network {
  fn connect() {}
}

mod client {
  fn connect() {}
}
```

On peut appeler les fonctions `connect` en tapant respectivement `network::connect()` et `client::connect()`

Pas besoin de tout mettre dans `src/lib.rs`, on peut créer un fichier et y faire référence dans `lib.rs`.
Par exemple,

```rust
// Fichier src/lib.rs
pub mod client;
pub mod network;

// Rust part chercher les fichiers src/client.rs et tout ce qu'il y a dans le dossier network


//Fichier src/client.rs
pub fn connect() {}

//Fichier src/network/mod.rs
fn connect() {}
pub mod server;

//Fichier src/network/server.rs
pub fn connect() {}

//Fichier src/main.rs
extern crate communicator;
fn main() {
    communicator::client::connect();
}

```

### Règles pour le nommage des modules

- Si le module `foo` n'a pas de sous-modules, le mettre dans `src/foo.rs`
- Sinon, créer un sous-dossier et les mettre dans le fichier `src/foo/mod.rs`

On met des `pub` partout pour que les fonctions soient publiques et surtout pour éviter le warning `unused code`.

### Appeler des fonctions

```rust
pub mod a {
  pub mod series {
    pub mod of {
      pub fn nested_modules() {}
    }
  }
}

fn main() {
  a::series::of::nested_modules();
}
```

Ou sinon pour faire plus clair

```rust
use a::series::of::nested_modules;;
fn main() {
  nested_modules();
}
```

On peut importer un module entier en utlisant un _glob_ avec `*`

```rust
enum TrafficLight {
  Red,
  Yellow,
  Green,
}
use TrafficLight::*;

fn main() {
  let red = Red;
  let yellow = Yellow;
  let green = Green;
}
```

On peut accéder à un module parent avec le mot-clé `super`

```rust
pub mod client;
pub mod network;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        super::client::connect();
    }
}
```

## Collections

Vecteurs: stocke un nombre variables de valeurs les unes à côté des autres
_Strings_: collection de caractères
_Hash maps_: associe une clé et une valeur

### Vecteurs

#### Initialisation

```rust
let v: Vec<i32> = Vec::new();

//Ou en utilisant l'inférence de type
let v = vec![1, 2, 3];
```

#### Modification

```rust
let mut v = Vec::new();
v.push(5);
v.push(6);
v.push(7);
v.push(8);
```

#### Accès

```rust
let v = vec![1, 2, 3, 4, 5];
let x: &i32 = &v[2];
let y: Option<&i32> = v.get(2);
```

La différence entre les deux méthodes est que Rust throw un `panic!` avec `x = &v[index]` si l'index est hors-limites.
Avec `y = v.get(index)` aucune erreur, y vaut tout simplement `None`

#### Références invalides

```rust
let mut v = vec![1, 2, 3, 4, 5];
let first = &v[0];
v.push(6);
```

Ce code a l'air valide mais en fait, ajouter un élément à la fin d'un vecteur requiert d'allouer un nouveau bloc de mémoire et copier les anciens éléments sur ce nouvel espace.
Dans ce cas, il n'y a pas assez de place pour mettre tous les éléments à l'endroit du vecteur. La référence au premier élément pointerait sur de la mémoire non allouée.

#### Stocker des valeurs de types différents

Pour ça, on utilise un enum.

```rust
enum SpreadsheetCell {
  Int(i32),
  Float(f64),
  Text(String),
}

let row = vec![
  SpreadsheetCell::Int(3),
  SpreadsheetCell::Text(String::from("blue")),
  SpreadsheetCell::Float(10.12),
]
```

### Strings

#### Création

```rust
let mut s = String::new();
let s = String::from("Hello world");
```

#### Concaténation

```rust
let mut s = String::from("foo");
s.push_str("bar");

let mut s1 = String::from("foo");
let s2 = String::from("bar");
s1.push_str(&s2);

let s1 = String::from("tic");
let s2 = String::from("tac");
let s3 = String::from("toe");
let s = format!("{}-{}-{}", s1, s2, s3);
```

#### Accès

Contrairement aux autres langages de programmation, on ne peut **pas** accéder à un caractère d'une _string_ via un index - ni utiliser les _slices_

#### Boucler sur une string

```rust
for c in "abcd".chars() {
  println!("{}", c);
}
```

### Hash maps

Exemple: créer une _hash map_ qui associe le nom d'une équipe (String) et son score.

```rust
use std::collections::HashMap;

let mut scores = HashMap::new();

scores.insert(String::from("Blue"), 10);
scores.insert(String::from("Yellow"), 50);

//Ou plus rapidement
let teams = vec![String::from("Blue"), String::from("Yellow")];

let initial_scores = vec![10, 50];
let scores: HashMap<_, _> = teams.iter().zip(initial_scores.iter()).collect();
```

#### Hash maps et ownership

```rust
use std::collections::HashMap;

let field_name = String::from("Favorite color");
let field_value = String::from("Blue");

let mut map = HashMap::new();
map.insert(field_name, field_value);

/*
 * À ce stade, field_name et field_value ne sont plus utilisables parce que les valeurs en elles-mêmes ne sont pas insérées dans la map, on y fait juste référence.

 Du coup, ça implique que les variables soient constamment dans le même scope que la map.
*/
```
